package org.boosey.parking.space.commands;

import java.util.UUID;

import javax.management.RuntimeErrorException;
import com.google.inject.Inject;
import org.boosey.parking.common.servicebase.CommandServiceBase;
import org.boosey.parking.common.servicebase.CommandServiceInterface;
import org.boosey.parking.common.entities.Space;
import org.boosey.parking.common.exceptions.ItemExistsException;
import org.boosey.parking.common.events.CreateSpaceCommandEvent;
import org.boosey.parking.space.query.MutinySpaceQueryGrpc;
import org.boosey.parking.space.query.SpaceNumberExistsRequest;
import org.boosey.parking.space.query.SpaceNumberExistsReply;
import io.quarkus.grpc.runtime.annotations.GrpcService;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.infrastructure.Infrastructure;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SpaceCommandsImpl extends CommandServiceBase<Space> implements CommandServiceInterface<Space>{

	@Inject @GrpcService("space-query-service") MutinySpaceQueryGrpc.MutinySpaceQueryStub spaceQuery;

    @Override
    public UUID create(Space s) throws ItemExistsException {
			
		CreateSpaceCommandEvent createSpaceCommandEvent = new CreateSpaceCommandEvent();

        return createSpaceCommandEvent.with(this.getNewItemId().toString(), s.number).emit();    
    }

	@Override
	public void init() {

		
	}

	public Boolean spaceNumberExists(Space s) {

		return false;
		// spaceQuery.spaceNumberExists(SpaceNumberExistsRequest.newBuilder().setNumber(s.number).build())
		// 	.subscribeOn(Infrastructure.getDefaultWorkerPool())
		// 	.subscribe().with(b -> {
		// 			return b;

		// 	}, 
		// 	failure -> {
		// 		log.error("Error getting list: ", failure);
		// 		throw new RuntimeException("Error getting list");
		// 	});


	}

}