package org.boosey.parking.space.commands;

import java.io.IOException;
import javax.enterprise.context.control.ActivateRequestContext;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.protobuf.Message;
import com.google.protobuf.util.JsonFormat;
import org.boosey.parking.common.entities.Space;
import org.boosey.parking.common.exceptions.ItemExistsException;
import io.grpc.stub.StreamObserver;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.infrastructure.Infrastructure;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Singleton
public class SpaceCommands extends SpaceCommandsGrpc.SpaceCommandsImplBase {

    @Inject SpaceCommandsImpl spaceCommands;

    @Override
    @ActivateRequestContext
    public void addSpace(SpaceAddRequest request, StreamObserver<SpaceAddReply> responseObserver) {

        Uni.createFrom().item(() -> {
			try {
                Space s = toPojoBean(Space.class, request);
                return spaceCommands.create(s);
			} catch (IOException e) {
                e.printStackTrace();
                return null;
			} catch (ItemExistsException e) {
                e.printStackTrace();
                return null;
			}
        })
        .subscribeOn(Infrastructure.getDefaultWorkerPool())
        .subscribe().with(
            uuid -> {
                log.info("Added - command uuid: {}", uuid);
                SpaceAddReply sar = SpaceAddReply.newBuilder().setCommandUUID(uuid.toString()).build();

                log.error("onNext");
                responseObserver.onNext(sar);
                responseObserver.onCompleted();
                log.error("Completed");
            },

            failure -> {
                log.error("Error adding space: ", failure);
                throw new RuntimeException("Error adding space");
            });
    }

    /**
     * Converting ProtoBean objects into POJO objects
     *
     * @param destPojoClass Class types of target POJO objects
     * @param sourceMessage ProtoBean object instance with data
     * @param <PojoType> Class type paradigm for target POJO objects
     * @return
     * @throws IOException
     */
    public static <PojoType> PojoType toPojoBean(Class<PojoType> destPojoClass, Message sourceMessage)
        throws IOException {
        if (destPojoClass == null) {
            throw new IllegalArgumentException
                ("No destination pojo class specified");
        }
        if (sourceMessage == null) {
            throw new IllegalArgumentException("No source message specified");
        }
        String json = JsonFormat.printer().print(sourceMessage);
        return new Gson().fromJson(json, destPojoClass);
    }

    /**
     * Converting POJO objects into ProtoBean objects
     *
     * @param destBuilder Builder class for the target Message object
     * @param sourcePojoBean POJO objects with data
     * @return
     * @throws IOException
     */
    public static void toProtoBean(Message.Builder destBuilder, Object sourcePojoBean) throws IOException {
        if (destBuilder == null) {
            throw new IllegalArgumentException
                ("No destination message builder specified");
        }
        if (sourcePojoBean == null) {
            throw new IllegalArgumentException("No source pojo specified");
        }
        String json = new Gson().toJson(sourcePojoBean);
        JsonFormat.parser().merge(json, destBuilder);
    }    

}