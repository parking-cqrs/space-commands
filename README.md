# Start Mongo
oc new-app --name='parking-event-store' mongo:4.0 

# Start Postgres
Use Postgres Operator
name = parkingspacequerydatabase
db name = parkingspacequerydatabase
password = space_service_account_password
user = space_service_account

# Start Kafka
Use Strimzi Operator
name = parking-events

# Build image
docker build . -f src/main/docker/Dockerfile.maven -t boosey/space-commands:latest

# Push image to Docker Hub
docker push  boosey/space-commands:latest

# Create new app - DC, Svc, ImageStream
oc new-app boosey/space-commands:latest
- DC: imagePullPolicy: Always
- Add in ImageStream: reference: true

# Route
oc expose svc/space-commands

# Update combined command
docker build . -f src/main/docker/Dockerfile.maven -t boosey/space-commands:latest \
  && docker push boosey/space-commands:latest \
  && oc rollout latest dc/space-commands
